//
//  BZScrollView.h
//  Budzma
//
//  Created by Maxim on 1/19/13.
//  Copyright (c) 2013 TonyDev. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import <AVFoundation/AVFoundation.h>
#import <UIKit/UIKit.h>

@interface BZScrollView : UIScrollView <AVAudioPlayerDelegate> {
    __strong NSDictionary * item_;
}

@property (nonatomic, retain) AVAudioPlayer *player;

- (id)initWithItem:(NSDictionary *)item;
- (void)fadeVolumeDown:(AVAudioPlayer *)pl;

@end
