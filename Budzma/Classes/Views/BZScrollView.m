//
//  BZScrollView.m
//  Budzma
//
//  Created by Maxim on 1/19/13.
//  Copyright (c) 2013 TonyDev. All rights reserved.
//

#import "BZScrollView.h"

@implementation BZScrollView
@synthesize player;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)initWithItem:(NSDictionary *)item {
    item_ = item;
    UIImageView * imageV = [item objectForKey:@"image"];
    UITextView * textV = [item objectForKey:@"text"];
    
    textV.dataDetectorTypes=UIDataDetectorTypeLink;
    textV.editable=NO;
    
    CGFloat height = 0;
    if (imageV) {
        height += imageV.frame.size.height;
    }
    if (textV) {
        height += textV.frame.size.height;
    }
    
    CGRect frame = CGRectMake([[UIScreen mainScreen] bounds].size.width * [[item objectForKey:@"id"] intValue], 0, [[UIScreen mainScreen] bounds].size.width, height);
    if (imageV) {
        frame.size.height += 10;
    }
    
    self = [self initWithFrame:frame];
    if (self) {
        imageV.tag = 101;
        textV.tag = 102;
        textV.hidden = YES;
        [self addSubview:imageV];
        
        UIButton * play = [UIButton buttonWithType:UIButtonTypeCustom];
        play.frame = CGRectMake(334, 462, 100, 100);
        UIImageView * playButton = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"play"]];
        [playButton setAlpha:0.6];
        playButton.tag = 10001;
        [play addSubview:playButton];
        
        UITapGestureRecognizer *playStop =
        [[UITapGestureRecognizer alloc] initWithTarget:self
                                                action:@selector(playStop:)];
        [play addGestureRecognizer:playStop];
        
        CGRect faderFrame = imageV.frame;
        faderFrame.size.height = textV.frame.size.height+20;
        if(faderFrame.size.height<50)
            faderFrame.size.height=50;
        UIView * fader = [[UIView alloc] initWithFrame:faderFrame];
        fader.backgroundColor = [UIColor colorWithWhite:1.000 alpha:0.500];
        fader.layer.cornerRadius = 5;
        fader.layer.masksToBounds = YES;
        fader.hidden = YES;
        fader.tag = 104;
        [self addSubview:fader];
    
        UIImageView * image_ = [[UIImageView alloc] initWithImage:[UIImage imageNamed:[item_ objectForKey:@"photo"]]];
        image_.frame = CGRectMake(10, 10, 110, 110);
        image_.layer.cornerRadius=55;
        image_.layer.masksToBounds = YES;
        
        [fader addSubview:image_];
        [fader addSubview:textV];
        
        play.tag = 103;
        //play.hidden = YES;
        [self addSubview:play];
        
        [self addSubview:textV];
        
        UITapGestureRecognizer *singleTouch =
        [[UITapGestureRecognizer alloc] initWithTarget:self
                                                      action:@selector(singleTouch:)];
        UIButton * btnInfo = [UIButton buttonWithType:UIButtonTypeInfoLight];
        btnInfo.frame = CGRectMake(self.frame.size.width - 50, 20, 20, 20);
        [btnInfo addGestureRecognizer:singleTouch];
        [self addSubview:btnInfo];
        
        self.backgroundColor = [UIColor colorWithWhite:0.681 alpha:1.000];
    }
    return self;
}

- (void)playStop:(UITapGestureRecognizer *)reconizer {
    UIButton * btt = (UIButton *)[reconizer view];
    [btt setAlpha:0.6];
    UIImageView * img = (UIImageView *)[btt viewWithTag:10001];
    
    CATransition * transition = [CATransition animation];
    [transition setDuration:0.1];
    [transition setType:kCATransitionFade];
    [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn]];
    
    [img.layer addAnimation:transition forKey:kCATransitionReveal];
    
    if (!player.playing)
    {
        NSString *path = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%@", [item_ objectForKey:@"sound"]] ofType:@"mp3"];
        NSURL *fileURL = [[NSURL alloc] initFileURLWithPath:path];
        
        if (fileURL)
        {
            AVAudioPlayer *newPlayer =
            [[AVAudioPlayer alloc] initWithContentsOfURL:fileURL
                                                   error:nil];
            
            self.player = newPlayer;
            [player prepareToPlay];
            
            img.image = [UIImage imageNamed:@"pause"];
            player.delegate = self;
            [player play];
        }
    } else {
        img.image = [UIImage imageNamed:@"play"];
        [self fadeVolumeDown:player];
    }
}

- (void)singleTouch:(UITapGestureRecognizer *)recognizer {  
    CATransition * transition = [CATransition animation];
    [transition setDuration:0.2];
    [transition setType:kCATransitionFade];
    [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn]];
    
    UIImageView * image = (UIImageView *)[self viewWithTag:101];
    UILabel * text = (UILabel *)[self viewWithTag:102];
    UIButton * play = (UIButton *)[self viewWithTag:103];
    UIView * fader = (UIView *)[self viewWithTag:104];
    [image.layer addAnimation:transition forKey:kCATransitionReveal];
    [text.layer addAnimation:transition forKey:kCATransitionReveal];
    [play.layer addAnimation:transition forKey:kCATransitionReveal];
    [fader.layer addAnimation:transition forKey:kCATransitionReveal];
    
    if (text.hidden) {
        fader.hidden = NO;
        play.hidden = NO;
        text.hidden = NO;
    } else {
        fader.hidden = YES;
        //play.hidden = YES;
        text.hidden = YES;
    }
}

- (void)fadeVolumeDown:(AVAudioPlayer *)pl
{
	pl.volume -= 0.05f;
	if (pl.volume < 0.05f)
	{
		[pl stop];
	}
	else
	{
		[self performSelector:@selector(fadeVolumeDown:) withObject:pl afterDelay:0.1f];
	}
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
 
 
 
*/

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag {
    UIButton * btt = (UIButton *)[self viewWithTag:103];
    UIImageView * img = (UIImageView *)[btt viewWithTag:10001];
    
    CATransition * transition = [CATransition animation];
    [transition setDuration:0.1];
    [transition setType:kCATransitionFade];
    [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn]];
    
    [img.layer addAnimation:transition forKey:kCATransitionReveal];
    
    img.image = [UIImage imageNamed:@"play"];
}

@end