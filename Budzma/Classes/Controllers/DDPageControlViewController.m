//
//  DDPageControlViewController.m
//  DDPageControl
//
//  Created by Damien DeVille on 1/14/11.
//  Copyright 2011 Snappy Code. All rights reserved.
//

#import "DDPageControlViewController.h"
#import "DDPageControl.h"

#define ARC4RANDOM_MAX	0x100000000


@implementation DDPageControlViewController

@synthesize scrollView ;


- (void)dealloc
{
	scrollView = nil ;
}

- (void)viewDidLoad
{
	[super viewDidLoad] ;
	
	int numberOfPages = 10 ;
	
	// define the scroll view content size and enable paging
	[scrollView setPagingEnabled: YES] ;
	[scrollView setContentSize: CGSizeMake(scrollView.bounds.size.width * numberOfPages, scrollView.bounds.size.height)] ;
	
	// programmatically add the page control
	pageControl = [[DDPageControl alloc] init] ;
	[pageControl setCenter: CGPointMake(self.view.center.x, self.view.bounds.size.height-30.0f)] ;
	[pageControl setNumberOfPages: numberOfPages] ;
	[pageControl setCurrentPage: 0] ;
	[pageControl addTarget: self action: @selector(pageControlClicked:) forControlEvents: UIControlEventValueChanged] ;
	[pageControl setDefersCurrentPageDisplay: YES] ;
	[pageControl setType: DDPageControlTypeOnFullOffEmpty] ;
	[pageControl setOnColor: [UIColor colorWithWhite: 0.9f alpha: 1.0f]] ;
	[pageControl setOffColor: [UIColor colorWithWhite: 0.7f alpha: 1.0f]] ;
	[pageControl setIndicatorDiameter: 15.0f] ;
	[pageControl setIndicatorSpace: 15.0f] ;
	[self.view addSubview: pageControl] ;
    UIScrollView*ScrView=nil;
    UITextView*Text=nil;
	for (int i = 0 ; i < numberOfPages ; i++)
	{
        ScrView=[[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, scrollView.frame.size.width/numberOfPages, scrollView.frame.size.height*2)];
        ScrView.pagingEnabled = YES;
        scrollView.scrollEnabled = YES;
        [scrollView addSubview:ScrView];
        [scrollView setBackgroundColor:[UIColor whiteColor]];
        Text=[[UITextView alloc]initWithFrame:CGRectMake(0, 0, 240, 400)];
        Text.text=@"lol";
        [scrollView addSubview:Text];
        scrollView.contentSize = CGSizeMake(scrollView.frame.size.width * numberOfPages, 1000);
        scrollView.directionalLockEnabled=YES;
        Text.editable=NO;
    }
}

- (void)viewDidUnload
{
	
}


#pragma mark -
#pragma mark DDPageControl triggered actions

- (void)pageControlClicked:(id)sender
{
	DDPageControl *thePageControl = (DDPageControl *)sender ;
	
	// we need to scroll to the new index
	[scrollView setContentOffset: CGPointMake(scrollView.bounds.size.width * thePageControl.currentPage, scrollView.contentOffset.y) animated: YES] ;
}


#pragma mark -
#pragma mark UIScrollView delegate methods

- (void)scrollViewDidScroll:(UIScrollView *)aScrollView
{
	CGFloat pageWidth = scrollView.bounds.size.width ;
    float fractionalPage = scrollView.contentOffset.x / pageWidth ;
	NSInteger nearestNumber = lround(fractionalPage) ;
	
	if (pageControl.currentPage != nearestNumber)
	{
		pageControl.currentPage = nearestNumber ;
		
		// if we are dragging, we want to update the page control directly during the drag
		if (scrollView.dragging)
			[pageControl updateCurrentPageDisplay] ;
	}
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)aScrollView
{
	// if we are animating (triggered by clicking on the page control), we update the page control
	[pageControl updateCurrentPageDisplay] ;
}



- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return (interfaceOrientation == UIInterfaceOrientationPortrait) ;
}

- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning] ;
}

@end
