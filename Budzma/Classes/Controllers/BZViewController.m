//
//  BZViewController.m
//
//
//  Created by Maxim on 1/19/13.
//  Copyright (c) 2013 TonyDev. All rights reserved.
//

#import "BZViewController.h"

@interface BZViewController ()

@end

@implementation BZViewController
@synthesize scrollView;
@synthesize player;

- (id)initWithCoder:(NSCoder *)aDecoder
{
    
    if((self=[super initWithCoder:aDecoder]))
    {
        data_ = [NSMutableArray array];
        [self loadData];
        numberOfPages = data_.count + 2;
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];

    [scrollView setPagingEnabled: YES] ;
    //[scrollView setContentSize: CGSizeMake(scrollView.bounds.size.width * numberOfPages, scrollView.bounds.size.height)];

    pageControl = [[DDPageControl alloc] init] ;
    [pageControl setNumberOfPages: numberOfPages] ;
    [pageControl setCurrentPage: 0] ;
    [pageControl addTarget: self action: @selector(pageControlClicked:) forControlEvents: UIControlEventValueChanged] ;
    [pageControl setDefersCurrentPageDisplay: YES] ;
    [pageControl setType: DDPageControlTypeOnFullOffEmpty] ;
    [pageControl setOnColor: [UIColor colorWithWhite: 0.9f alpha: 1.0f]] ;
    [pageControl setOffColor: [UIColor colorWithWhite: 0.7f alpha: 1.0f]] ;
    [pageControl setIndicatorDiameter: 15.0f] ;
    [pageControl setIndicatorSpace: 15.0f];
    [pageControl setCenter: CGPointMake(self.view.center.x, self.view.bounds.size.height-30.0f)] ;
    [self.view addSubview: pageControl] ;
    
    UIView * startView = [[BZStartViewController alloc] initWithNibName:@"BZStartViewController" bundle:nil].view;
    startView.frame = CGRectMake(0, 0, startView.frame.size.width, startView.frame.size.height);
    [scrollView addSubview:startView];
    
    for (int i = 0 ; i < numberOfPages - 2; i++)
    {
        BZScrollView * scrView=[[BZScrollView alloc] initWithItem:data_[i]];
        scrView.tag = i + 1;
        
        scrView.pagingEnabled = YES;
        scrollView.scrollEnabled = YES;
        [scrollView addSubview:scrView];
        [scrollView setBackgroundColor:[UIColor colorWithWhite:0.681 alpha:1]];
        scrollView.contentSize = CGSizeMake(scrollView.frame.size.width * numberOfPages, 0);
        scrollView.directionalLockEnabled=YES;
    }
    UIView * endView = [[BZEndViewController alloc] initWithNibName:@"BZEndViewController" bundle:nil].view;
    endView.frame = CGRectMake((numberOfPages - 1) * [[UIScreen mainScreen] bounds].size.width, 0, startView.frame.size.width, startView.frame.size.height);
    [scrollView addSubview:endView];


}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark -
#pragma mark DDPageControl triggered actions

- (void)pageControlClicked:(id)sender
{
	DDPageControl *thePageControl = (DDPageControl *)sender ;
	
	// we need to scroll to the new index
	[scrollView setContentOffset: CGPointMake(scrollView.bounds.size.width * thePageControl.currentPage, scrollView.contentOffset.y) animated: YES] ;
}

#pragma mark -
#pragma mark UIScrollView delegate methods
NSUInteger fader_ = 0;
- (void)scrollViewDidScroll:(UIScrollView *)aScrollView
{
    if (fader_ != [pageControl currentPage]) {
        NSUInteger flag = [pageControl currentPage] - fader_;
        
        if (([pageControl currentPage] - flag) != 0 && ([pageControl currentPage] - flag) != numberOfPages) {
            CATransition * transition = [CATransition animation];
            [transition setDuration:0.2];
            [transition setType:kCATransitionFade];
            [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn]];
            
            BZScrollView * currentScroll1 = (BZScrollView *)[scrollView viewWithTag:[pageControl currentPage] - flag];
            UIImageView * image = (UIImageView *)[currentScroll1 viewWithTag:101];
            UILabel * text = (UILabel *)[currentScroll1 viewWithTag:102];
            UIButton * play = (UIButton *)[currentScroll1 viewWithTag:103];
            UIView * fader = (UIView *)[currentScroll1 viewWithTag:104];
            

            
            UIImageView * img = (UIImageView *)[play viewWithTag:10001];
            img.image = [UIImage imageNamed:@"play"];
            


            [currentScroll1 fadeVolumeDown:currentScroll1.player];
            [image.layer addAnimation:transition forKey:kCATransitionReveal];
            [text.layer addAnimation:transition forKey:kCATransitionReveal];
            [play.layer addAnimation:transition forKey:kCATransitionReveal];
            [fader.layer addAnimation:transition forKey:kCATransitionReveal];
            
            //text.hidden = YES;
            //play.hidden = YES;
            //fader.hidden = YES;
        }
        
        fader_ = [pageControl currentPage];
    }
    
	CGFloat pageWidth = scrollView.bounds.size.width ;
    float fractionalPage = scrollView.contentOffset.x / pageWidth ;
	NSInteger nearestNumber = lround(fractionalPage) ;
	
	if (pageControl.currentPage != nearestNumber)
	{
		pageControl.currentPage = nearestNumber ;
		
		// if we are dragging, we want to update the page control directly during the drag
		if (scrollView.dragging) {
            [pageControl updateCurrentPageDisplay];
        }
	}
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)aScrollView
{
	// if we are animating (triggered by clicking on the page control), we update the page control
	[pageControl updateCurrentPageDisplay] ;
}

#pragma mark - Model
- (void)loadData {
    NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"data"
                                                          ofType:@"plist"];
    NSData *plistData = [NSData dataWithContentsOfFile:plistPath];
    
    NSPropertyListFormat format;
	NSError *error = nil;
    id plist = [NSPropertyListSerialization propertyListWithData:plistData options:NSPropertyListMutableContainersAndLeaves format:&format error:&error];
    
    if (!error) {        
        if (plist) {
            for (NSDictionary *item in plist) {
                NSMutableDictionary * dictionary = [NSMutableDictionary dictionary];
                
                UIImage * image = nil;
                NSString * stringImage = [item objectForKey:@"image"];
                if (stringImage && ![stringImage isEqualToString:@""]) {
                    image = [UIImage imageNamed:stringImage];
                    if(image){
                        __strong UIImageView * imageV = [[UIImageView alloc] initWithImage:image];
                        
                        CGFloat i = [[UIScreen mainScreen] bounds].size.width / image.size.width;
                        CGRect frame = CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, imageV.frame.size.height * i);
                    
                        imageV.frame = frame;
                    
                        [dictionary setValue:imageV forKey:@"image"];
                    }
                }

                
                NSString * text = [item objectForKey:@"text"];
                if (text) {
                    UIImageView * image = [dictionary objectForKey:@"image"];
                    
                    CGRect textFrame = image.frame;
                    textFrame.origin.y += 10;
                    textFrame.origin.x += 110;
                    textFrame.size.width -= 140;
                    __strong UITextView * textV = [[UITextView alloc] initWithFrame:textFrame];
                    textV.font = [UIFont systemFontOfSize:20.0];
                    textV.text = text;
                    textV.backgroundColor = [UIColor clearColor];
                    
                    textV.textAlignment = UIBaselineAdjustmentAlignCenters;
                    textV.dataDetectorTypes=UIDataDetectorTypeLink;
                    [textV sizeToFit];
                    [dictionary setValue:textV forKey:@"text"];
                }
                
                [dictionary setValue:[item objectForKey:@"id"] forKey:@"id"];
                [dictionary setValue:[item objectForKey:@"sound"] forKey:@"sound"];
                [dictionary setValue:[item objectForKey:@"photo"] forKey:@"photo"];
                [data_ addObject:dictionary];
            }
        }
	}
	else {
		NSLog(@"error: %@", error);
	}
}
@end
