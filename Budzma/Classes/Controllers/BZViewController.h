//
//  BZViewController.h
//  Budzma
//
//  Created by Maxim on 1/19/13.
//  Copyright (c) 2013 TonyDev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <DDPageControl/DDPageControl.h>
#import <QuartzCore/QuartzCore.h>

#import <MediaPlayer/MediaPlayer.h>

#import "BZScrollView.h"
#import "BZStartViewController.h"
#import "BZEndViewController.h"

@interface BZViewController : UIViewController<UIScrollViewDelegate>
{
    NSUInteger numberOfPages;
    DDPageControl *pageControl;
    NSMutableArray * data_;
}
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (nonatomic, retain) AVAudioPlayer *player;

@end
