//
//  BZEndViewController.h
//  Budzma
//
//  Created by Alexey Oshevnev on 1/19/13.
//  Copyright (c) 2013 TonyDev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BZEndViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *endViewLabel;

@end
