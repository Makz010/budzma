//
//  BZEndViewController.m
//  Budzma
//
//  Created by Alexey Oshevnev on 1/19/13.
//  Copyright (c) 2013 TonyDev. All rights reserved.
//

#import "BZEndViewController.h"

@interface BZEndViewController ()

@end

@implementation BZEndViewController
@synthesize endViewLabel;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"endView" ofType:@"plist"];
    NSData *plistData = [NSData dataWithContentsOfFile:plistPath];
    
    NSPropertyListFormat format;
	NSError *error = nil;
    id plist = [NSPropertyListSerialization propertyListWithData:plistData options:NSPropertyListMutableContainersAndLeaves format:&format error:&error];
    
    endViewLabel.text=[plist objectForKey:@"label"];
    endViewLabel.textColor= [UIColor blackColor];

    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
