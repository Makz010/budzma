//
//  BZAppDelegate.h
//  Budzma
//
//  Created by Maxim on 1/19/13.
//  Copyright (c) 2013 TonyDev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BZAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
